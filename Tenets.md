# The Tenets of Toast and Tech:

- make EVERYONE welcome, enthusiastically
- make ALL questions welcome, enthusiastically
- everyone is learning, everyone is teaching
- there is no single path, there are as many paths as people attending, as many paths as needed
- you don't need a project or tech work to attend, just come
- you don't need any experience to attend, just come
- support everyone who shows up
- Make it safe to learn, safe to ask questions, safe to show up.

Possibly the MOST important tenant of all:

#ToastAndTech can happen *anywhere*, run by *anyone*; all it takes is letting folks know about it, and showing up to hold the space

