The Toast and Tech project is a resource for informal, locally-run, and inclusive study groups.

Nominally based around learning tech, the intent is to provide an inclusive, welcoming, and safe space for underserved people in the tech community: Blacks, Indigenous, People of Colour, People with Diabilities, LGBTQ+ folks, non-binary people, and women.

Learning is better with others.

This repo is intended for organizing the project, such as it is, and keeping notes and information available to anyone who might want to start up a study group.
